import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { Button, Card, Col, Container, Row } from 'react-bootstrap';

class Home extends React.Component {
  state = {
    availableCoins: [
      2000, 5000, 10000, 20000, 50000
    ],
    totalCoin: 0,
    Products: [
      {
        'name': 'Biskuit',
        'qty': 3,
        'price': 6000
      },
      {
        'name': 'Chips',
        'qty': 3,
        'price': 8000
      },
      {
        'name': 'Oreo',
        'qty': 3,
        'price': 10000
      },
      {
        'name': 'Tango',
        'qty': 3,
        'price': 12000
      },
      {
        'name': 'Cokelat',
        'qty': 3,
        'price': 15000
      }
    ],
    insertCoin: true,
    selectProduct: false,
    Carts: [],
    submitProductStatus: false
  };

  insertCoin(coin: number) {
    this.setState((state: any) => ({
      totalCoin: state.totalCoin + coin,
    }))

  }

  finishCoin() {
    if (this.state.totalCoin >= 6000) {
      this.setState({
        insertCoin: false,
        selectProduct: true,
        submitProductStatus: true
      })
    } else {
      alert('Jumlah uang Anda tidak cukup')
    }
  }

  warningEmptyProduct(name: string) {
    alert(`Jumlah Produk  ${name} Habis`)
  }

  takeProduct(name: string, productPrice: number) {
    if (this.state.totalCoin >= productPrice) {
      let ProductsTarget = this.state.Products;
      let emptyProduct = false
      ProductsTarget.map((v, k) => {
        if (v.name == name) {
          if (v.qty > 0) {
            ProductsTarget[k].qty = v.qty - 1
          } else {
            this.warningEmptyProduct(name);
            emptyProduct = true
          }
        }
      })
      this.setState((state: any) => ({
        Carts: emptyProduct ? state.Carts : [...state.Carts, { name: name, qty: 1 }],
        totalCoin: emptyProduct ? state.totalCoin : state.totalCoin - productPrice,
        Products: ProductsTarget
      }))
    } else {
      alert('Jumlah uang Anda tidak cukup')
    }
  }

  submitProduct() {
    alert(`total kembalian Anda${this.state.totalCoin}`)
  }

  render() {
    return (
      <Container>
        <Row>
          <h1>Vending Machine</h1>
          <h2>Total Saldo {this.state.totalCoin}</h2>
          {
            this.state.availableCoins.map((v, k) => {
              return (
                <Col key={k}>
                  <Card >
                    <Button onClick={() => this.insertCoin(v)} disabled={!this.state.insertCoin}>{v}</Button>
                  </Card>
                </Col>
              )
            })
          }
        </Row>
        <Row className='mt-3'>
          <Col>
            <Button onClick={() => this.finishCoin()} variant="info">Submit Money</Button>
          </Col>
        </Row>
        <Row >
          <h3 className='mt-5'>Products</h3>
          {
            this.state.Products.map((v, k) => {
              return (
                <Col className='mt-3' key={k}>
                  <Button onClick={(e) => this.takeProduct(v.name, v.price)} disabled={!this.state.selectProduct}>{v.name} : {v.price} ({v.qty})</Button>
                </Col>
              )
            })
          }
        </Row>
        <Row>
          <Col>
            {this.state.Carts && this.state.Carts.map((v, k) => {
              return (
                <>
                  <span key={k}>{v?.name} - {v?.qty}</span><br />
                </>
              )
            })}
          </Col>
          <Col xs={12}>
            <Button className='mt-3' onClick={() => this.submitProduct()} disabled={!this.state.submitProductStatus}>Submit Product</Button>
          </Col>
        </Row>
      </Container >
    )
  }
}

export default Home
